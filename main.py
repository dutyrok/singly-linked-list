class Link:
    def __init__(self, value):
        self.value = value
        self.next_link = None


class SingleLinkedList:
    def __init__(self):
        self.__first_item = None
        self.__length = 0

    def __len__(self):
        return self.__length

    def __iter__(self):
        self.__current_position = self.__first_item
        return self

    def __next__(self):
        if self.__current_position is not None:
            current_value = self.__current_position
            self.__current_position = self.__current_position.next_link
            return current_value.value

        else:
            raise StopIteration

    def add(self, *values):
        """Insert several values in list beginning"""
        for value in values[::-1]:
            self.__add(value)

    def __add(self, value):
        """Insert one value in list beginning"""
        if self.__length == 0:
            self.__first_item = Link(value)
        else:
            new_link = Link(value)
            new_link.next_link = self.__first_item
            self.__first_item = new_link
        self.__length += 1

    def __getitem__(self, item):
        current_item = 0
        for link_value in self:
            if current_item == item:
                return link_value
            else:
                current_item += 1

        raise IndexError

    def __delitem__(self, key):
        if key >= self.__length:
            raise IndexError

        if key == 0:
            self.__first_item = self.__first_item.next_link
        else:
            self.__delete_not_first_link(key)
        self.__length -= 1
    
    def __delete_not_first_link(self, key):
        parent_link: Link = self.__first_item
        for number_of_link in range(1, len(self)):
            if key == number_of_link:
                parent_link.next_link = parent_link.next_link.next_link
            else:
                parent_link = parent_link.next_link

    def __str__(self):
        result_str = str()
        for value in self:
            result_str += value.__str__() + ", "
        return result_str[:-2]


lst = SingleLinkedList()
lst.add(9, 87, 98, 12, 1, 2)
print(f"Длина списка: {len(lst)}")
print(lst)

del lst[5]

print(f"Длина списка: {len(lst)}")
print(lst)

del lst[0]

print(f"Длина списка: {len(lst)}")
print(lst)

del lst[lst.__len__()-1]

print(f"Длина списка: {len(lst)}")
print(lst)

lst.add(5, 4, 2)

print(f"Длина списка: {len(lst)}")
print(lst)

